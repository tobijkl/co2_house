BOARD_W = 65; // 3A+ 
BOARD_H = 56; // 3A+
HOLE_W = 58;
HOLE_H = 49;
PCB_THICKNESS = 1.5;
BOTTOM_CLEARANCE = 4;
TOP_CLEARNACE = 10;
SIDE_CLEARANCE = 4;
HOLE_DIAM = 2.5;

$fn = 50;

module 3A() {
    translate([0,0,PCB_THICKNESS/2])
    difference() {
        color("green") cube([BOARD_W, BOARD_H, PCB_THICKNESS], center=true);
        hole_pattern(HOLE_W, HOLE_H)  {
            cylinder(d=HOLE_DIAM, h=PCB_THICKNESS*2, center=true);
        }
    }
    USB_POWER();
    SD();
}

module USB_POWER(DO_CUTOUT=false) {
    color("orange") translate([BOARD_W/2-14.5,BOARD_H/2-6+2,PCB_THICKNESS]) {
        if (DO_CUTOUT) {
           translate([-2,0,-1]) cube([8+4,6+10,3+2]);
        } else {
            cube([8,6,3]);
        }
    }
}

module SD(DO_CUTOUT=false) {
    color("orange")  translate([BOARD_W/2-13,-6,-2]) {
        if (DO_CUTOUT) {
            translate([-1, -1, -1]) cube([16+10,12+4,2+2]);
        } else {
            cube([16,12,2]);
        }
    }
}

module hole_pattern(W, H) {
    translate([+W/2,+H/2,0]) children();
    translate([-W/2,+H/2,0]) rotate([0,0,90]) children();
    translate([-W/2,-H/2,0]) rotate([0,0,180]) children();
    translate([+W/2,-H/2,0]) rotate([0,0,270]) children();
}

BASE_HEIGHT = 15;
WALL_THICKNESS = 1.6;
LEDGE_HEIGHT = 4;

module base() {
    color("darkblue")
    difference() {
        translate([0,0,BASE_HEIGHT/2]) {
            difference() {
                minkowski() {
                    cube(
                        [BOARD_W+2*SIDE_CLEARANCE, BOARD_H+2*SIDE_CLEARANCE, 1],
                        center=true
                    );
                    cylinder(h=BASE_HEIGHT-1, r=WALL_THICKNESS, center=true);
                }
                translate([0,0,WALL_THICKNESS]) minkowski() {
                    cube(
                        [BOARD_W+2*SIDE_CLEARANCE-2*WALL_THICKNESS, BOARD_H+2*SIDE_CLEARANCE-2*WALL_THICKNESS, 1],
                        center=true
                    );
                    cylinder(h=BASE_HEIGHT-1+1, r=WALL_THICKNESS, center=true);
                }
                
                translate([0,0,BASE_HEIGHT-LEDGE_HEIGHT]) translate([0,0,-BASE_HEIGHT/2+LEDGE_HEIGHT/2]) difference() {minkowski() {
                    cube(
                        [BOARD_W+2*SIDE_CLEARANCE+1, BOARD_H+2*SIDE_CLEARANCE+1, 1],
                        center=true
                    );
                    cylinder(h=LEDGE_HEIGHT-1+1, r=WALL_THICKNESS, center=true);
                }
                translate([0,0,0]) minkowski() {
                    cube(
                        [BOARD_W+2*SIDE_CLEARANCE-1*WALL_THICKNESS, BOARD_H+2*SIDE_CLEARANCE-1*WALL_THICKNESS, 1],
                        center=true
                    );
                    cylinder(h=LEDGE_HEIGHT, r=WALL_THICKNESS, center=true);
                }
                }
                
            }
        }
        translate([0,0,WALL_THICKNESS+BOTTOM_CLEARANCE]) {
            USB_POWER(DO_CUTOUT=true);
        }
        translate([0,0,WALL_THICKNESS+BOTTOM_CLEARANCE]) {
            SD(DO_CUTOUT=true);
        } 
    }
    hole_pattern(HOLE_W, HOLE_H)  {
            difference() {
                cylinder(d=5, h=BOTTOM_CLEARANCE+WALL_THICKNESS);
                cylinder(d=0.75*HOLE_DIAM, h=BOTTOM_CLEARANCE+WALL_THICKNESS+1);
            }
    }
}

FLOOR_HEIGHT = 30;

module floors() {
    color("white")
    translate([0,0,FLOOR_HEIGHT/2]) {
        difference() {
            minkowski() {
                cube(
                   [BOARD_W+2*SIDE_CLEARANCE+WALL_THICKNESS, BOARD_H+2*SIDE_CLEARANCE+WALL_THICKNESS, 1],
                   center=true
                );
                cylinder(h=FLOOR_HEIGHT-1, r=WALL_THICKNESS, center=true);
            }
            translate([0,0,0]) minkowski() {
                cube([
                    BOARD_W+2*SIDE_CLEARANCE-WALL_THICKNESS,
                    BOARD_H+2*SIDE_CLEARANCE-WALL_THICKNESS,
                1], center=true);
                cylinder(h=FLOOR_HEIGHT, r=WALL_THICKNESS, center=true);
            }
            translate([-BOARD_W/2-SIDE_CLEARANCE,-20,0]) window();
            translate([-BOARD_W/2-SIDE_CLEARANCE,0,0]) window();
            translate([-BOARD_W/2-SIDE_CLEARANCE,20,0]) window();
            
            translate([-(-BOARD_W/2-SIDE_CLEARANCE),-20,0]) window();
            translate([-(-BOARD_W/2-SIDE_CLEARANCE),0,0]) window();
            translate([-(-BOARD_W/2-SIDE_CLEARANCE),20,0]) window();
        }
    }
}

LOWER_ROOF_HEIGHT = 2;
ROOF_HEIGHT = 30;

module lower_roof() {
    color("white")
    rotate_copy() {
        translate([(BOARD_W+2*SIDE_CLEARANCE+WALL_THICKNESS)/2,0,0]) difference() {
            hull() {
                translate([0,0,ROOF_HEIGHT]) rotate([0,90,0]) {
                    cylinder(h=WALL_THICKNESS, r=2);
                }
                translate([
                    0,
                    -(BOARD_H+2*SIDE_CLEARANCE+2*WALL_THICKNESS)/2
                    ,0
                ]) {
                    cube([
                        WALL_THICKNESS,
                        (BOARD_H+2*SIDE_CLEARANCE+2*WALL_THICKNESS),
                        LOWER_ROOF_HEIGHT
                    ]);
                }
            }
            
            translate([-0.75*WALL_THICKNESS,0,0.4*ROOF_HEIGHT]) rotate([0,90,0]) {
                difference() {
                    cylinder(h=2*WALL_THICKNESS, r=5);
                    translate([-1,-6,0]) { 
                        cube([2,2*6,2*WALL_THICKNESS]); 
                    }
                }
            }
        }
        
        translate([
            -(BOARD_W+2*SIDE_CLEARANCE+WALL_THICKNESS)/2,
            -(BOARD_H+2*SIDE_CLEARANCE+2*WALL_THICKNESS)/2,
            0
        ]) {
            translate([0,-WALL_THICKNESS/2,0]) cube([
            BOARD_W+2*SIDE_CLEARANCE+WALL_THICKNESS,
            WALL_THICKNESS,
            LOWER_ROOF_HEIGHT]);
        }
    }
}

module rotate_copy() {
    children();
    rotate([0,0,180]) children();
}

module window() {
    D = 3.5;
    W = 5;
    translate([0,+D,+D]) cube([5,W,W], center=true);
    translate([0,+D,-D]) cube([5,W,W], center=true);
    translate([0,-D,+D]) cube([5,W,W], center=true);
    translate([0,-D,-D]) cube([5,W,W], center=true);
}

ROOF_OVERHANG = 5;
ROOF_LENGTH = 52;

module roof(side=1) {
    color("red")
    translate([0,0,ROOF_HEIGHT+2]) rotate([43*side,0,0])
    translate([0,-1*side*ROOF_LENGTH/2,WALL_THICKNESS/2]) {
        cube([
            BOARD_W+2*SIDE_CLEARANCE+2*WALL_THICKNESS+2*ROOF_OVERHANG,
            ROOF_LENGTH,
            WALL_THICKNESS
        ], center=true);
    }
}

module roof_clean(side=1) {
    color("red")
    translate([0,0,ROOF_HEIGHT+2]) rotate([43*side,0,0])
    translate([0,-1*side*ROOF_LENGTH/2,-WALL_THICKNESS/2-10]) {
        cube([
            BOARD_W+2*SIDE_CLEARANCE+2*WALL_THICKNESS+2*ROOF_OVERHANG,
            ROOF_LENGTH,
            WALL_THICKNESS+20
        ], center=true);
    }    
}

module schornstein() {
    translate([0,0,6]) difference() {
        cube([8,8,12],center=true);
        translate([0,0,0]) cylinder(h=12+2, d=5.5 ,center=true);
        translate([0,0,-1]) cylinder(h=12, d=6.5 ,center=true);
    }
}

module schornstein_cutout() {
    translate([0,0,6]) difference() {
        translate([0,0,-2]) cylinder(h=12, d=6 ,center=true);
    }
}

module schornstein_layout() {
    translate([20,0,0]) {
        translate([+10,-8,23]) children();
        translate([  0,-8,23]) children();
        translate([-10,-8,23]) children();
    }
}

module roof_left() {
    difference() {
        union() {
            difference() {
                roof(side=1);
                schornstein_layout() schornstein_cutout();
            }
            schornstein_layout() schornstein();
        }
        roof_clean(side=1);
    }
}

module roof_right() {
    difference() {
        union() {
            difference() {
                roof(side=-1);
                gaube_layout() gaube_cutout();
            }
            gaube_layout() gaube();
        }
        roof_clean(side=-1);
    }
}


module gaube() {
    difference() {
        translate([0,-4,0]) cube([20,20,20], center=true);
        gaube_window();
    }
}

module gaube_window() {
    translate([4.5,0,4.5]) cube([8,30,8], center=true);
    translate([-4.5,0,4.5]) cube([8,30,8], center=true);    
}

module gaube_cutout() {
    translate([0,-4,0]) cube([20,20,20], center=true);
}

module gaube_layout() {
    W = 20;
    H = 24;
    Z = 11;
    translate([-W, H, Z]) children();
    translate([W, H, Z]) children();
}

module roof_cap() {
    W = 3;
    H = 3;

    color("red") union() {  
    hull() {
        translate([0,0,H]) rotate([0,90,0]) cylinder(
            h=BOARD_W+2*SIDE_CLEARANCE+2*WALL_THICKNESS+2*ROOF_OVERHANG,
            d=WALL_THICKNESS,center=true
        );
        translate([0,W,0]) rotate([0,90,0]) cylinder(
            h=BOARD_W+2*SIDE_CLEARANCE+2*WALL_THICKNESS+2*ROOF_OVERHANG,
            d=WALL_THICKNESS,center=true
        );
    }
    hull() {
        translate([0,0,H])rotate([0,90,0]) cylinder(
            h=BOARD_W+2*SIDE_CLEARANCE+2*WALL_THICKNESS+2*ROOF_OVERHANG,
            d=WALL_THICKNESS,center=true
        );
        translate([0,-W,0])rotate([0,90,0]) cylinder(
            h=BOARD_W+2*SIDE_CLEARANCE+2*WALL_THICKNESS+2*ROOF_OVERHANG,
            d=WALL_THICKNESS,center=true
        );
    }
    translate([0,0,1.2]) rotate([45,0,0]) {
        cube([
            BOARD_W+2*SIDE_CLEARANCE+2*WALL_THICKNESS+2*ROOF_OVERHANG
            ,WALL_THICKNESS,WALL_THICKNESS], center=true
        );
    }}
}

module fan() {
    translate([0,0,4]) difference() {
        cube([30,30,8], center=true);
        hole_pattern(24,24) cylinder(h=10,d=3,center=true);
    }
}

module bme680() {
    difference() {
        cube([14,16,3], center=true);
        translate([5,+5,0]) cylinder(h=4, d=2, center=true);
        translate([5,-5,0]) cylinder(h=4, d=2, center=true);
    }
    translate([7+2.5,0,0]) cube([5,5,3], center=true);
    translate([-7+2.5/2,0,-9/2-3/2]) cube([2.5,16,9], center=true);
}

module co2sensor() {
    
}

//fan();
//bme680();
//co2sensor();

module fan_carrier() {
    W=HOLE_W;
    H=HOLE_H;
    HEIGHT=WALL_THICKNESS;
    
    translate([0,0,WALL_THICKNESS/2]) difference() {
        union() {
        hull() {
                translate([+W/2,+H/2,0]) rotate([0,0,  0])
                cylinder(d=5,h=HEIGHT, center=true);
                translate([-W/2,-H/2,0]) rotate([0,0,180])
                cylinder(d=5,h=HEIGHT, center=true);
            }
            hull() {
                translate([-W/2,+H/2,0]) rotate([0,0, 90])
                cylinder(d=5,h=HEIGHT, center=true);
                translate([+W/2,-H/2,0]) rotate([0,0,270])
                cylinder(d=5,h=HEIGHT, center=true);
            }
            cube([32,32,HEIGHT],center=true);
            
            translate([21,-27,-HEIGHT/2])
            cube([11,12,HEIGHT]);
        }
        cylinder(d=27,h=2*HEIGHT,center=true);
        
        hole_pattern(HOLE_W, HOLE_H) 
        cylinder(d=3, h=2*HEIGHT, center=true);
        
        hole_pattern(24, 24) 
        cylinder(d=2, h=2*HEIGHT, center=true);
        
        translate([16,-30,-HEIGHT])
        cube([10,10,2*HEIGHT]);
    }
}

module sensor_carrier() {
    W=HOLE_W;
    H=HOLE_H;
    HEIGHT=WALL_THICKNESS;
    
    translate([0,0,WALL_THICKNESS/2]) difference() {
        union() {
        hull() {
            translate([+W/2,+H/2,0]) rotate([0,0,  0])
            cylinder(d=5,h=HEIGHT, center=true);
            translate([-W/2,-H/2,0]) rotate([0,0,180])
            cylinder(d=5,h=HEIGHT, center=true);
        }
        hull() {
            translate([-W/2,+H/2,0]) rotate([0,0, 90])
            cylinder(d=5,h=HEIGHT, center=true);
            translate([+W/2,-H/2,0]) rotate([0,0,270])
            cylinder(d=5,h=HEIGHT, center=true);
        }
        cylinder(d=32,h=HEIGHT,center=true);
        }
        hole_pattern(HOLE_W, HOLE_H) 
        cylinder(d=3, h=2*HEIGHT, center=true);
        cylinder(d=26,h=2*HEIGHT,center=true);
    }
}

STANDOFF_HEIGHT = 15;

module standoff(HEIGHT=15) {
    translate([0,0,HEIGHT/2]) difference() {
        cylinder(d=5, h=HEIGHT, center=true);
        cylinder(d=3, h=2*HEIGHT, center=true);
    }
}


//fan_carrier();

module standoff_lower() {
    difference() {
        union() {
            cylinder(h=12, d=7);
            translate([0,0,-4]) cylinder(h=4, d=3);
        }
        translate([0,0,12-3]) cylinder(h=3, d=6);
    }
}

//standoff_lower();


module standoff_upper() {
    difference() {
        union() {
            cylinder(h=14, d=9);
            cylinder(h=14+3, d=3);
        }
        cylinder(h=6, d=7);
    }
}

base();
translate([0,0,WALL_THICKNESS+BOTTOM_CLEARANCE]) 3A();
translate([0,0,BASE_HEIGHT-LEDGE_HEIGHT-1]) floors();
translate([0,0,BASE_HEIGHT-LEDGE_HEIGHT-1+FLOOR_HEIGHT]) lower_roof();
translate([0,0,BASE_HEIGHT-LEDGE_HEIGHT-1+FLOOR_HEIGHT]) roof_left();
translate([0,0,BASE_HEIGHT-LEDGE_HEIGHT-1+FLOOR_HEIGHT]) roof_right();
translate([0,0,BASE_HEIGHT-LEDGE_HEIGHT-1+FLOOR_HEIGHT+ROOF_HEIGHT+2]) roof_cap();

import os
import time
import json
import datetime
import board
import busio
import digitalio
import adafruit_bme680
import mh_z19

GOOD_AIR = 600
OKAY_AIR = 1000

INTERVAL = 10

# initialize LEDs
led_green = digitalio.DigitalInOut(board.D1)
led_green.switch_to_output()
led_green.value = False

led_yellow = digitalio.DigitalInOut(board.D7)
led_yellow.switch_to_output()
led_yellow.value = False

led_red = digitalio.DigitalInOut(board.D8)
led_red.switch_to_output()
led_red.value = False

# initialize bme680
i2c = busio.I2C(board.SCL, board.SDA)
bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c)

# initialize co2-sensor

# helper function
def set_leds(green, yellow, red):
    led_green.value = green
    led_yellow.value = yellow
    led_red.value = red

# LED animation
for i in range(0, 1):
    set_leds(True, False, False)
    time.sleep(0.5)
    set_leds(False, True, False)
    time.sleep(0.5)
    set_leds(False, False, True)
    time.sleep(0.5)

set_leds(True, True, True)
time.sleep(0.5)
set_leds(False, False, False)

# Loop
try:
    f = open('data.csv', 'a')
    while True:
        print('Temperature: {} degrees C'.format(bme680.temperature))
        print('Gas: {} ohms'.format(bme680.gas))
        print('Humidity: {}%'.format(bme680.humidity))
        print('Pressure: {}hPa'.format(bme680.pressure))

        mh_output = mh_z19.read_all()

        co2 = mh_output["co2"]
        co2_temperature = mh_output["temperature"]

        print('Temperature: {} degrees C'.format(co2_temperature))
        print('CO2: {} ppm'.format(co2))

        s = str(datetime.datetime.now())+',{:.1f},{:d},{:.1f},{:.0f},{:d},{:d}'.format( \
            bme680.temperature, \
            int(bme680.gas), \
            bme680.humidity, \
            bme680.pressure, \
            int(co2), \
            int(co2_temperature))

        f.write(s+"\n")
        f.flush()
        os.fsync(f)

        if co2 <= GOOD_AIR:
            set_leds(True, False, False)
        elif co2 <= OKAY_AIR:
            set_leds(False, True, False)
        else:
            set_leds(False, False, True)

        time.sleep(INTERVAL)

except Exception as e:
    print(e)

finally:
    set_leds(False, False, False)
    f.close()
